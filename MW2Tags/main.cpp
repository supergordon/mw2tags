#include <windows.h>
#include "CHackshield.h"
/*
00582D5C   /0F85 A3000000   JNZ iw4mp.00582E05                       ; nop dat
00582D62   |8B15 141C7F00   MOV EDX,DWORD PTR DS:[7F1C14]            ; iw4mp.0646A510
00582D68   |8D0CFF          LEA ECX,DWORD PTR DS:[EDI+EDI*8]
00582D6B   |8D0448          LEA EAX,DWORD PTR DS:[EAX+ECX*2]
00582D6E   |8D2C40          LEA EBP,DWORD PTR DS:[EAX+EAX*2]
00582D71   |8B4424 14       MOV EAX,DWORD PTR SS:[ESP+14]
00582D75   |3B42 10         CMP EAX,DWORD PTR DS:[EDX+10]
00582D78   |8D2CAD 38B77D00 LEA EBP,DWORD PTR DS:[EBP*4+7DB738]
00582D7F   |7F 1E           JG SHORT iw4mp.00582D9F
00582D81   |E8 5AFDFFFF     CALL iw4mp.00582AE0
00582D86   |84C0            TEST AL,AL
00582D88   |74 15           JE SHORT iw4mp.00582D9F                  ; nop dat
00582D8A   |807D 08 00      CMP BYTE PTR SS:[EBP+8],0


*/

DWORD WINAPI setBreakpoints(LPVOID) 
{
	g_pHackshield->Initialize();	
	return 1;
}

BOOL WINAPI DllMain(HANDLE, DWORD dwReason, LPVOID)
{
	if(dwReason == DLL_PROCESS_ATTACH) {
		CreateThread(0, 0, setBreakpoints, 0, 0, 0);
	}

	return TRUE;
}