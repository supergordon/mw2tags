#define _WIN32_WINNT 0x0501
#include <windows.h>
#include <tlhelp32.h>
#include "CHackshield.h"

CHackshield* g_pHackshield = new CHackshield();

DWORD dwNameTag1 = 0;
DWORD dwNameTag2 = 0;

CHackshield::CHackshield()
{

}

void SetBPs(bool toggle);

LONG CALLBACK ExpectionHandler(PEXCEPTION_POINTERS ExceptionInfo)
{
	if(ExceptionInfo->ExceptionRecord->ExceptionCode == EXCEPTION_SINGLE_STEP) {
		ExceptionInfo->ContextRecord->EFlags |= (1 << 16);


		if((DWORD)ExceptionInfo->ExceptionRecord->ExceptionAddress == dwNameTag1) {
			ExceptionInfo->ContextRecord->Eip += 6;
		}

		if((DWORD)ExceptionInfo->ExceptionRecord->ExceptionAddress == dwNameTag2) {
			ExceptionInfo->ContextRecord->Eip += 2;
		}

		return EXCEPTION_CONTINUE_EXECUTION;
	}
	return EXCEPTION_CONTINUE_SEARCH; 
}


void SetBPs(bool toggle) 
{
	CONTEXT Context = {CONTEXT_DEBUG_REGISTERS};

	if(1) {
		Context.Dr0 = dwNameTag1;//0x582D5C;
		Context.Dr1 = dwNameTag2;//0x582D88;

		Context.Dr7 = 0x00000001;
		Context.Dr7 |= 0x00000004;
	}
	else {
		Context.Dr0 = 0;
		Context.Dr1 = 0;
		Context.Dr2 = 0;
		Context.Dr3 = 0;
		Context.Dr7 = 0x00000000;
	}


	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, GetCurrentProcessId());
	THREADENTRY32 th32 = {0};
	th32.dwSize = sizeof(THREADENTRY32);

	Thread32First(hSnapshot, &th32);
	do {
		if(th32.th32OwnerProcessID == GetCurrentProcessId() && th32.th32ThreadID != GetCurrentThreadId()) {
			DWORD tid = (DWORD)th32.th32ThreadID;
			HANDLE thread = OpenThread(THREAD_ALL_ACCESS , 0, tid);
			if(thread) {
				SuspendThread(thread);
				SetThreadContext(thread,&Context);
				ResumeThread(thread);
				CloseHandle(thread);
			}
		}
	} while(Thread32Next(hSnapshot, &th32));

	CloseHandle(hSnapshot);
}

bool bDataCompare(const BYTE* pData, const BYTE* bMask, const char* szMask)
{
	for(;*szMask;++szMask,++pData,++bMask)
		if(*szMask=='x' && *pData!=*bMask ) 
			return false;
	return (*szMask) == NULL;
}

DWORD dwFindPattern(DWORD dwAddress,DWORD dwLen,BYTE *bMask,char * szMask)
{
	for(DWORD i=0; i < dwLen; i++)
		if( bDataCompare( (BYTE*)( dwAddress+i ),bMask,szMask) )
			return (DWORD)(dwAddress+i);

	return 0;
}

#include <iostream>
void CHackshield::Initialize()
{
	AddVectoredExceptionHandler(1, &ExpectionHandler);

	MEMORY_BASIC_INFORMATION mbi = { 0 };
	DWORD dwMain = (DWORD)GetModuleHandle(0);
	VirtualQuery((void*)(dwMain+0x1000), &mbi, sizeof(mbi));


	dwNameTag1 = dwFindPattern((DWORD)mbi.BaseAddress, mbi.RegionSize, (PBYTE)"\x0F\x85\x00\x00\x00\x00\x8B\x15\x00\x00\x00\x00\x8D\x0C\xFF\x8D", "xx????xx????xxxx");
	dwNameTag2 = dwNameTag1 + 0x2C;

	SetBPs(true);
}

